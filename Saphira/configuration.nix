# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.efi.efiSysMountPoint = "/boot/EFI";

  # Installs the latest Kernel
  boot.kernelPackages = pkgs.linuxPackages_latest;

  networking.hostName = "saphira-nixos"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "America/Chicago";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.utf8";

  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # Nvidia GPU
  services.xserver = 
     { videoDrivers = [ "nvidia" ];

  config = ''
      Section "Device"
          Identifier "nvidia"
          Driver "nvidia"
          BusID "PCI:10:0:0"
      EndSection
    '';
    screenSection = ''
      Option         "metamodes" "DP-2: nvidia-auto-select +1920+0 {ForceCompositionPipeline=On, ForceFullCompositionPipeline=On}, DP-4: nvidia-auto-select +0+0 {ForceCompositionPipeline=On, ForceFullCompositionPipeline=On}"
    '';  
};

  hardware.nvidia.nvidiaSettings = true;
  hardware.opengl.enable = true;

  # Enable NTFS Support
  boot.supportedFilesystems = [ "ntfs" ];
  
  # smb shares
  services.gvfs.enable = true;
  
  # QT5CT
  environment.variables.QT_QPA_PLATFORMTHEME = "qt5ct";

  # Enable the LXQT Desktop Environment.
  services.xserver.displayManager.sddm.enable = true;
  services.xserver.displayManager.sddm.autoNumlock = true;
  services.xserver.desktopManager.lxqt.enable = true;
  services.xserver.displayManager.defaultSession = "none+dwm";

    # i3wm
  programs.qt5ct.enable = true;
  services.xserver.windowManager.i3 = {
     enable = true;
     extraPackages = with pkgs; [
        i3status-rust
        i3lock
        xss-lock
        st
        dmenu
        volumeicon
        dwmblocks
        feh
        sxhkd
        picom
	qt5ct
        libsForQt5.oxygen-icons5
        dracula-theme
     ];
  };

  # DWM
  services.xserver.windowManager.dwm.enable = true;

  # Configure keymap in X11
  services.xserver = {
    layout = "us";
    xkbVariant = "";
  };

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable sound with pipewire.
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.kell = {
    isNormalUser = true;
    initialPassword = "Kell";
    description = "kell";
    extraGroups = [ "networkmanager" "wheel" "video" "audio" ];
  };

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # Allow Flatpaks
  # flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
  services.flatpak.enable = true;
  xdg.portal.extraPortals = [ pkgs.lxqt.xdg-desktop-portal-lxqt ];

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    neofetch
    htop
    gimp
    kdenlive
    notepadqq
    wget
    git
    curl
    harfbuzz.dev
    xorg.libXft.dev
    firefox
    gparted
    brave
    unrar
    unzip
    p7zip
    mpv
    exa
    appimage-run
    libgdiplus # steam dep
    (st.overrideAttrs (oldAttrs: rec {
    src = /home/kell/git/projects/dwm-config/st ;
    buildInputs = oldAttrs.buildInputs ++ [ harfbuzz ];
    }))
    (dwm.overrideAttrs (oldAttrs: rec {
    src = /home/kell/git/projects/dwm-config/config ;
    buildInputs = oldAttrs.buildInputs ++ [ harfbuzz ];
    }))
    (dwmblocks.overrideAttrs (oldAttrs: rec {
    src = /home/kell/git/projects/dwm-config/dwmblocks ;
    buildInputs = oldAttrs.buildInputs ++ [ harfbuzz ];
    }))
  ];
  
  # Installs Vim and becomes default editor
  programs.vim.defaultEditor = true;

  # enable Steam
  programs.steam = {
    enable = true;
    remotePlay.openFirewall = true; # Open ports in the firewall for Steam Remote Play
    dedicatedServer.openFirewall = true; # Open ports in the firewall for Source Dedicated Server
  };

  # enable Java
  programs.java.enable = true;
  
  # Fixes some programs not saving
  programs.dconf.enable = true;

  # enable nm-applet
  programs.nm-applet.enable = true;

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leavecatenate(variables, "bootdev", bootdev)
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.05"; # Did you read the comment?

}
